# ADTreeWriter
**Maintainer**: Simon Spannagel (simon.spannagel@cern.ch)  
**Status**: Immature  
**Input**: PixelHit  

### Description
Module to write ROOT trees in the format expected by the analysis of Timepix3 data taken at the AD.

### Parameters
* `file_name`: Name of the output ROOT file, defaults to the detector name.

### Usage

```
[ADTreeWriter]
file_name = "output_trees.root"

```

