/**
 * @file
 * @brief Definition of ADTreeWriter module
 * @copyright Copyright (c) 2019 CERN and the Allpix Squared authors.
 * This software is distributed under the terms of the MIT License, copied verbatim in the file "LICENSE.md".
 * In applying this license, CERN does not waive the privileges and immunities granted to it by virtue of its status as an
 * Intergovernmental Organization or submit itself to any jurisdiction.
 *
 * Contains minimal dummy module to use as a start for the development of your own module
 *
 * Refer to the User's Manual for more details.
 */

#include <string>

#include <TFile.h>
#include <TTree.h>

#include "core/config/Configuration.hpp"
#include "core/geometry/DetectorModel.hpp"
#include "core/messenger/Messenger.hpp"
#include "core/module/Module.hpp"

#include "objects/PixelHit.hpp"

namespace allpix {
    /**
     * @ingroup Modules
     * @brief Module to write out ROOT trees of pixel hits
     *
     * More detailed explanation of module
     */
    class ADTreeWriterModule : public SequentialModule {
    public:
        /**
         * @brief Constructor for ADTreeWiter module
         * @param config Configuration object for this module as retrieved from the steering file
         * @param messenger Pointer to the messenger object to allow binding to messages on the bus
         * @param detector Pointer to the detector for this module instance
         */
        ADTreeWriterModule(Configuration& config, Messenger* messenger, const std::shared_ptr<Detector>& detector);

        /**
         * @brief Opens the file to write the objects to
         */
        void initialize() override;

        /**
         * @brief Writes the objects to the trees
         */
        void run(Event* event) override;

        /**
         * @brief Write the output file
         */
        void finalize() override;

    private:
        // General module members
        std::shared_ptr<Detector> detector_;
        Messenger* messenger_;

        // Struct to store tree and information for each detector
        struct sensor_data {
            static constexpr int MAXHITS = (1 << 16);
            TTree* data_tree_; // no unique_ptr, ROOT takes ownership
            TTree* time_tree_; // no unique_ptr, ROOT takes ownership
            Int_t run_;
            UInt_t track_;
            Int_t nhits_;
            UInt_t cluster_tot_;
            Float_t cluster_x_;
            Float_t cluster_y_;
            UInt_t columns_[MAXHITS];
            UInt_t rows_[MAXHITS];
            UInt_t toa_[MAXHITS];
            UInt_t ftoa_[MAXHITS];
            UInt_t value_[MAXHITS];
            UInt_t time_[MAXHITS];
            Int_t pdg_codes_[MAXHITS];
            ULong64_t trig_time_;
        };
        sensor_data data_;

        // Output data file to write
        std::unique_ptr<TFile> output_file_;
        std::string output_path_;
    };
} // namespace allpix
